package com.simion.util;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Calendar;

/**
 * Clase que proporciona diferentes metodos utiles a lo largo del uso del programa.
 * Created by Simion on 04/03/2017.
 * @author Simion Cornel Bordean.
 * @version 1.8.0_121
 */
public class Util {

    /**
     * Metodo estatico que genera una ventana emergete con el icono de error.
     * @param titulo Titulo de la ventana emergente.
     * @param mensaje Mensaje a mostrar en la ventana emergente.
     */
    public static void error(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metod estatico que genera una ventana emergente con el icono de warnign.
     * @param titulo Titulo de la ventana emergente.
     * @param mensaje Mensaje a mostrar en la ventana emergente.
     */
    public static void warning(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Metod estatico que genera una ventana emergente con el icono de informacion.
     * @param titulo Titulo de la ventana emergente.
     * @param mensaje Mensaje a mostrar en la ventana emergente.
     */
    public static void info(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Metodo estatico que gestiona la hora y fecha actual.
     * @return String con el formato de la fecha y hora actual.
     */
    public static String getActualTime() {
        Calendar fecha = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        sb.append(fecha.get(Calendar.DATE)+"-");
        sb.append(fecha.get(Calendar.MONTH)+"-");
        sb.append(fecha.get(Calendar.YEAR)+"_");
        sb.append(fecha.get(Calendar.HOUR_OF_DAY)+"-");
        sb.append(fecha.get(Calendar.MINUTE)+"-");
        sb.append(fecha.get(Calendar.SECOND));
        return sb.toString();
    }

    /**
     * Metodo estatico que da formato a un numero de tipo long.
     * @param tamanio El numero con formato long a parsear.
     * @return String con el formato necesario.
     */
    public static String parseTamanio(long tamanio) {

        double result = 0;
        DecimalFormat formatter;
        if (tamanio >= (1024*1024)){
            formatter = new DecimalFormat("#.## MB");
            result = ((double) tamanio) / (1024*1024);
        }
        else if (tamanio >= 1024){
            formatter = new DecimalFormat("#.## KB");
            result = ((double) tamanio) / (1024);
        }
        else{
            result = tamanio;
            formatter = new DecimalFormat("#.## B");
        }
        return formatter.format(result);

    }
}

