package com.simion.base;

import com.simion.util.Util;

/**
 * Created by Simion on 05/03/2017.
 * Clase que getiona los archivos del directorio backup
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class Archivo {

    private String nombre;
    private String tamanio;

    /**
     * Constructor de la clase
     */
    public Archivo(){

    }

    /**
     * Metodo que devuelve el nombre del archvio
     * @return Nombre del archivo.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que asigna el nombre al fichero.
     * @param nombre del archivo.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve el tamaño del archivo de tipo long.
     * @return tamaño del archivo en long
     */
    public String getTamanio() {
        return tamanio;
    }

    /**
     * Mentodo que asigna el tamaño del archivo.
     * @param tamanio es el tamaño en long a asignar.
     */
    public void setTamanio(long tamanio) {
        this.tamanio = Util.parseTamanio(tamanio);
    }
}
