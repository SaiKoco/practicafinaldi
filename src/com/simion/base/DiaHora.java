package com.simion.base;

/**
 * Clase que guarda la informacion de dia, hora y minuto
 * Created by Simion on 05/03/2017.
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class DiaHora {

    private String dia;
    private String hora;
    private String min;

    /**
     * Constructor de la clase.
     * @param dia El día en String con formato MON, THU, ... , SAT, SUN
     * @param hora La hora en String en formato 00, 02, ... , 23, 24
     * @param min Los minutos en String en formato 00, 02, ... , 58, 59
     */
    public DiaHora(String dia, String hora, String min){
        this.dia = dia;
        this.hora = hora;
        this.min= min;
    }

    /**
     * Metodo que delvuel el día.
     * @return dia String con el formato dia
     */
    public String getDia() {
        return dia;
    }

    /**
     * Metodo que devuelve la hora
     * @return hora String con el formato hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * Metodo que devuelve los minutso
     * @return minutos String con el formato de los minutos.
     */
    public String getMin() {
        return min;
    }
}
