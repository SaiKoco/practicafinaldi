package com.simion.gui;

import javax.swing.*;
import java.awt.event.*;

/**
 * Clase ge gestiona una ventana emergente.
 * @author Simion Cornel Bordean.
 * @version 1.8.0_121
 */
public class AcercaDe extends JDialog {
    private JPanel contentPane;

    /**
     * Constructor de la clase
     */
    public AcercaDe() {
        setTitle("Acerca del Programa");
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Metodo que gestiona el boton de cancelar de la ventana emergente.
     */
    private void onCancel() {
        dispose();
    }
}
