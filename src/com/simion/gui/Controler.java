package com.simion.gui;

import com.simion.util.Util;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

/**
 * Clase gue se engarga de gestionar la informacion entre el modelo y el interfaz gráfico.
 * Created by Simion on 04/03/2017.
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class Controler implements ActionListener, ListSelectionListener {


    private final Model modelo;
    private final Ventana view;

    /**
     * Constructor de la clase.
     * @param modelo de tipo Modelo
     * @param view de tipo Ventana
     */
    public Controler(Model modelo, Ventana view) {
        this.modelo = modelo;
        this.view = view;

        addListeners();
    }

    /**
     * Metodo privado que se encarga de asignar los listeners a los
     * diferentes elementos del interfaz gráfico.
     */
    private void addListeners() {
        view.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                modelo.desconectar();
                super.windowClosing(e);
            }
        });
        view.conectarItem.addActionListener(this);
        view.autoBackUp.addActionListener(this);
        view.salirItem.addActionListener(this);
        view.crearBackUp.addActionListener(this);
        view.volcarBackUp.addActionListener(this);
        view.carpetaBackUp.addActionListener(this);
        view.contenidoBackUp.addActionListener(this);
        view.acercaDe.addActionListener(this);

        view.tbTabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsmTabla = view.tbTabla.getSelectionModel();
        lsmTabla.addListSelectionListener(this);
    }

    /**
     * Metodo que se encarga de gestionar los eventos generados por los
     * diferentes elementos de la ventana.
     * @param e Es el evento que se ha generado.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando){
            case "conectarItem":
                ConectarDialog conect = new ConectarDialog();
                conect.setVisible(true);
                if (conect.datos != null){
                    modelo.setDatosConexion(conect.getDatosConexion());
                    modelo.conectar();
                    view.dtmTabla.setNumRows(0);
                    for (String baseDatos: modelo.obtenerBBDDs()){
                        String[] fila = {baseDatos};
                        view.dtmTabla.addRow(fila);
                    }
                }
                break;
            case "autoBackUp":
                SelectTime dias = new SelectTime(modelo.obtenerConfigAutoBackUp());
                dias.setVisible(true);
                if (!dias.obtenerCancelar()){
                    modelo.configAutoBackUp(dias.listaDatos);
                }
                break;
            case "salirItem":
                modelo.desconectar();
                System.exit(0);
                break;
            case "crearBackUp":
                if (celdaSeleccionada()){
                    modelo.crearBackUp(obtenerBBDD());
                }
                else
                    Util.warning("Seleccionar fila", "Primero debe seleccionar una fila.");
                break;
            case "volcarBackUp":
                try {
                    String rutaFicheroBBDD = view.obtenerAbrir();
                    modelo.volcarBackUp(rutaFicheroBBDD);
                }catch (NullPointerException npe){
                    npe.printStackTrace();
                    Util.warning("Sin selección.", "No ha seleccionado ningún archivo.");
                }
                break;
            case "carpetaBackUp":
                try {
                    String ruta = view.obtenerDirectorio();
                    modelo.setRutaBackUps(ruta);
                    Util.info("Ruta cambiada", "Ruta cambiada al siguiente directorio: \n"+ruta);
                }catch (NullPointerException npe){
                    Util.warning("Error", "NO ha seleccionado un directorio válido.");
                }
                break;
            case "contenidoBackUp":

                JasperReport report = modelo.obtenerReport();
                try {
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, null,
                            new JRBeanCollectionDataSource(modelo.obtenerFicheros()));
                    String rutaFichero = modelo.obtenerRutaBackUp() + File.separator + "PDFReport"+Util.getActualTime()+".pdf";
                    JasperExportManager.exportReportToPdfFile(jasperPrint, rutaFichero);
                    Util.info("PDF Exportado", "El informe se ha realizado con exito. \n" +
                            "Ubicaion del archivo: "+rutaFichero);
                } catch (JRException e1) {
                    Util.warning("Report no generado", "El informe jasper no se ha generado.");
                }
                break;
            case "acercaDe":
                AcercaDe about = new AcercaDe();
                about.setVisible(true);
                break;
            default:
                break;
        }
    }

    /**
     * Metodo que se engarga de gestionar los eventos generados por la tabla al
     * seleccionar las diferentes filas de la tabla.
     * @param e Es el evento generado.
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        JFreeChart pieChart = ChartFactory
                .createPieChart("Tamaño tablas de "+obtenerBBDD(), modelo.obtenerSizeTablas(obtenerBBDD()),
                        false, true, false);
        view.chartPanel.setChart(pieChart);
        view.chartPanel.updateUI();

    }

    /**
     * Metodo privado que se encarga de obtener el nombre de la BBDD seleccionado en la tabla.
     * @return String con el nombre de la bbdd seleccionada.
     */
    private String obtenerBBDD() {
        return (String) view.tbTabla.getValueAt(view.tbTabla.getSelectedRow(), 0);
    }

    /**
     * Metodo privado que se engarga de comprobar si hay alguna celda seleccionada en la tabla.
     * @return valor booleano que idica si hay alguna celda seleccionada.
     */
    private boolean celdaSeleccionada() {
        for (int i = 0; i < view.dtmTabla.getRowCount(); i++){
            if (view.tbTabla.isRowSelected(i)){
                return true;
            }
        }
        return false;
    }
}
