package com.simion.gui;

import javax.swing.*;
import java.awt.event.*;

/**
 * Clase que gestiona una ventana emergete para recoger la informacion
 * de conexion con la base de datos.
 * @author  Simion Cornel Bordean.
 * @version 1.8.0_121
 */
public class ConectarDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    JTextField tfServidor;
    JTextField tfPuerto;
    JTextField tfUser;
    JPasswordField pfPasswd;
    String[] datos;

    /**
     * Metodo constructor de la clase.
     */
    public ConectarDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        pack();
        setLocationRelativeTo(null);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Metod privado que se encarga de gestionar el boton ok puslado.
     */
    private void onOK() {
        String servidor = tfServidor.getText();
        String puerto = tfPuerto.getText();
        String user = tfUser.getText();
        String passwd = "";
        for (char letra:pfPasswd.getPassword()){
            passwd += letra;
        }
        datos = new String[]{servidor, puerto, user, passwd};
        dispose();
    }

    /**
     * Metodo privado que se enraga de gestionar el boton cancelar pulsado.
     */
    private void onCancel() {
        datos = null;
        dispose();
    }

    /**
     * Metodo que devuelve los datos recogidos en la ventana emergente.
     * @return Vector de String con los datos recogidos.
     */
    public String[] getDatosConexion(){
        return datos;
    }
}
