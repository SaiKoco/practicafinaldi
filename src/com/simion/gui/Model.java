package com.simion.gui;

import com.simion.base.Archivo;
import com.simion.base.DiaHora;
import com.simion.util.Util;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.jfree.data.general.DefaultPieDataset;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * Clase que se encarga de gestionar informacion externa al programa.
 * Created by Simion on 04/03/2017.
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class Model {

    private Connection dbConection;
    private String host;
    private String puerto;
    private String user;
    private String passwd;
    private String rutaBackUps;
    private ArrayList<DiaHora> listaDatos;

    /**
     * Constructor de la clase que se encarga de inicializar los elementos
     * necearios.
     */
    public Model(){
        File backup = new File(System.getProperty("user.dir") + File.separator + "backup"+File.separator);
        if (!backup.exists()){
            backup.mkdir();
        }
        rutaBackUps = backup.getAbsolutePath();
        listaDatos = new ArrayList<>();
        for (int i = 0; i<7; i++){
            DiaHora diaHora = new DiaHora("d", String.valueOf(i), String.valueOf(i));
            listaDatos.add(diaHora);
        }

    }

    /**
     * Metodo que se encarga de conectar con la base de datos.
     */
    public void conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbConection = DriverManager.getConnection("jdbc:mysql://"+host+":"+puerto+"/", user, passwd);
        } catch (ClassNotFoundException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String mensaje = "Driver de MySQL no encontrado.";
            String titulo = "Driver MySQL";
            Util.error(titulo, mensaje);
        } catch (SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String mensaje = "Datos incorrectos para conectar.\nEste es el error: \n"+sw.toString();
            String titulo = "Conexion no realizada";
            Util.error(titulo, mensaje);
        }
    }

    /**
     * Metodo que se encarga de desconectar de la base de datos.
     */
    public void desconectar(){
        try {
            if (dbConection != null){
                dbConection.close();
            }
        } catch (SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String mensaje = "Desconexion no realizada. \nEste es el error: \n"+sw.toString();
            String titulo = "Desconexion no realizada";
            Util.error(titulo, mensaje);
        }
    }

    /**
     * Metodo que se encarga de obtener las BBDDs existentes en MySQL
     * @return Una lista con los nombres de las BBDDs existentes.
     */
    public ArrayList<String> obtenerBBDDs(){
        String orden = "show databases";
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<String> listaBBDDs = new ArrayList<>();
        try {
            ps = dbConection.prepareStatement(orden);
            rs = ps.executeQuery();
            while (rs.next()){
                listaBBDDs.add(rs.getString(1));
            }
            return listaBBDDs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Metodo que se encarga de crar un fichero .sql con la estructura e
     * informacion de una base de datos suministrada.
     * @param baseDatos Nombre de la BBDD a crear el BackUp
     */
    public void crearBackUp(String baseDatos) {

        try {
            Process process = Runtime.getRuntime()
                    .exec("mysqldump --opt --host " + host + " -u " + user + " -p" + passwd + " " + baseDatos);
            StringBuilder sb = new StringBuilder();
            sb.append(rutaBackUps+"\\");
            sb.append(baseDatos);
            sb.append("_");
            sb.append(Util.getActualTime());
            sb.append(".sql");
            PrintWriter printWriter = new PrintWriter(new FileWriter(sb.toString()));
            BufferedReader entrada = new BufferedReader(new InputStreamReader(process.getInputStream()));
            printWriter.println("CREATE DATABASE IF NOT EXISTS "+baseDatos+";");
            printWriter.println("USE "+baseDatos+";");
            String linea = entrada.readLine();
            while (linea != null){
                printWriter.println(linea);
                linea = entrada.readLine();
            }
            printWriter.close();
            Util.info("BackUp exitoso", "BackUp realizado con éxito");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de almacenar en memoria los datos necesarios
     * para conectar con mysql.
     * @param datosConexion Un vector con los datos necearios.
     */
    public void setDatosConexion(String[] datosConexion) {
        host = datosConexion[0];
        puerto = datosConexion[1];
        user = datosConexion[2];
        passwd = datosConexion[3];
    }

    /**
     * Metodo que se encarga de cargar desde un fichero .sql la estructrua y los datos
     * de una base de datos.
     * @param rutaBBDD Ruta del archivo SQL en String.
     */
    public void volcarBackUp(String rutaBBDD) {
        if (user == null || passwd == null){
            Util.warning("Conectar a MySQL", "Primero debe conectar a mysql.");
            return;
        }
        String[] comando = {"mysql", "-u"+user, "-p"+passwd, "-e", "source " + new File(rutaBBDD)};
        try {
            Process process = Runtime.getRuntime()
                    .exec(comando);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String linea;
            while ((linea = entrada.readLine()) != null){
                System.out.println(linea);
            }
            Util.info("BackUp importado.", "BackUp importado con exito.");
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String mensaje = "Error al volcar los datos. \nEste es el error: \n"+sw.toString();
            String titulo = "Error al volcar.";
            Util.error(titulo, mensaje);
        }
    }

    /**
     * Metodo que se encarga de obtener el tamañado que ocupan las tablas de una BBDD
     * en MySQL.
     * @param nombreBBDD Nombre de la base de datos.
     * @return DefaultPieDataset util para generar un JFreeChart
     */
    public DefaultPieDataset obtenerSizeTablas(String nombreBBDD) {
        String sentencia = "SELECT table_name Tabla,(data_length+index_length) " +
                "Tamaño FROM information_schema.tables WHERE table_schema=?";
        PreparedStatement ps;
        ResultSet rs;
        DefaultPieDataset listaTablas = new DefaultPieDataset();
        try {
            ps = dbConection.prepareStatement(sentencia);
            ps.setString(1, nombreBBDD);
            rs = ps.executeQuery();
            while (rs.next()){
                double tamanio = rs.getDouble(2);
                tamanio = tamanio / 1024;
                String nombreTabla = rs.getString(1) + " - " + tamanio + " KB";
                listaTablas.setValue(nombreTabla, tamanio);
            }
            return listaTablas;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Metodo que se encarga de modificar la ruta del directorio de los BackUps
     * @param rutaBackUps String que contiene la ruta completa del directorio.
     */
    public void setRutaBackUps(String rutaBackUps) {
        this.rutaBackUps = rutaBackUps;
    }

    /**
     * Metodo que se encarga de ejecutar las ordenes necesarias para realizar
     * la ejecucion programada de los backups.
     * @param listaDias Datos con los días y las horas especificas para ralizar los backups.
     */
    public void configAutoBackUp(ArrayList<DiaHora> listaDias) {
        listaDatos = listaDias;
        if (user == null || passwd == null){
            Util.warning("Conectar a MySQL", "Primero debe conectar a mysql.");
            return;
        }
        Process process;
        StringBuilder sb;
        for (DiaHora diaHora : listaDias){
            sb = new StringBuilder();
            sb.append("SCHTASKS /CREATE /TN backup");
            sb.append(diaHora.getDia());
            sb.append(" /TR \"mysqldump --opt -u"+user+" -p"+passwd+" -r "+rutaBackUps+"\\backup"+diaHora.getDia()+".sql\"");
            sb.append(" /SC WEEKLY /D "+diaHora.getDia());
            sb.append(" /ST "+diaHora.getHora()+":"+diaHora.getMin());
            sb.append(" /F");
            try {
                process = Runtime.getRuntime()
                        .exec(sb.toString());
                BufferedReader entrada = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String linea = entrada.readLine();
                if (linea != null){
                    Util.error("Error en la Modificacion", "La modificacion no se ha podido realizar.");
                    return;
                }
                while (linea != null){
                    System.out.println(linea);
                    linea = entrada.readLine();
                }
            } catch (IOException e) {
                Util.error("Error en la Modificacion", "La modificacion no se ha podido realizar.");
                e.printStackTrace();
                return;
            }
        }
        Util.info("Config. Modificada", "La configuracion de backUps ha sido modificada.");
    }

    /**
     * Metodo que se encarga de obtener la configuración actual de los días de los backups
     * @return una lista de tipo DiaHora con la información necesaria.
     */
    public ArrayList<DiaHora> obtenerConfigAutoBackUp() {
        return this.listaDatos;
    }

    /**
     * Metodo que se encarga de obtener la ruta de los los backups actual.
     * @return String con la ruta completa del directorio de BackUps.
     */
    public String obtenerRutaBackUp() {
        return rutaBackUps;
    }

    /**
     * Metodo que devuelve un report generado de una plantilla realizada con iReport 5.6.0
     * @return la plantilla generada de tipo JasperReport.
     */
    public JasperReport obtenerReport() {
        try {
            return (JasperReport) JRLoader.loadObject(ClassLoader.getSystemResource("archivoJasper.jasper"));
        } catch (JRException e) {
            Util.error("Error de archivo. ",
                    "No se ha podido generar el reporte. Archivo Jarper no encontrardo.");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Metodo que devuelve una lista de tipo Archivo con los datos que hay en la carpeta
     * de la realizacón de los backups.
     * @return lista de tipo Archivo con los datos obtenidos.
     */
    public Collection<Archivo> obtenerFicheros() {
        File f = new File(rutaBackUps);
        File[] ficheros = f.listFiles();
        Archivo archivo;
        ArrayList<Archivo> listaArchivos = new ArrayList<>();
        for (int i = 0; i < ficheros.length; i++){
            archivo = new Archivo();
            archivo.setNombre(ficheros[i].getName());
            archivo.setTamanio(ficheros[i].length());
            listaArchivos.add(archivo);
        }
        return listaArchivos;
    }
}
