package com.simion.gui;

import com.simion.base.DiaHora;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Clase que se encarga de gestionar una ventana emergente donde se recoge
 * la informacion necesario para realizar la programacion automatica de
 * copias de seguridad.
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class SelectTime extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    JComboBox cbHoraLunes;
    JComboBox cbMinLunes;
    JComboBox cbHoraMartes;
    JComboBox cbHoraMiercoles;
    JComboBox cbHoraJueves;
    JComboBox cbHoraViernes;
    JComboBox cbHoraSabado;
    JComboBox cbHoraDomingo;
    JComboBox cbMinMartes;
    JComboBox cbMinMiercoles;
    JComboBox cbMinJueves;
    JComboBox cbMinViernes;
    JComboBox cbMinSabado;
    JComboBox cbMinDomingo;

    ArrayList<DiaHora> listaDatos;
    private boolean cancelar;

    /**
     * Constructor de la clase que se encarga de inicializar y modificar los
     * parametros de los distintos elemntos de la ventana.
     * @param diaHoras Una lista con los datos neceario para ralizar las modificaciones.
     */
    public SelectTime(ArrayList<DiaHora> diaHoras) {
        cancelar = false;
        cbHoraLunes.setSelectedItem(diaHoras.get(0).getHora());
        cbMinLunes.setSelectedItem(diaHoras.get(0).getMin());

        cbHoraMartes.setSelectedItem(diaHoras.get(1).getHora());
        cbMinMartes.setSelectedItem(diaHoras.get(1).getMin());

        cbHoraMiercoles.setSelectedItem(diaHoras.get(2).getHora());
        cbMinMiercoles.setSelectedItem(diaHoras.get(2).getMin());

        cbHoraJueves.setSelectedItem(diaHoras.get(3).getHora());
        cbMinJueves.setSelectedItem(diaHoras.get(3).getMin());

        cbHoraViernes.setSelectedItem(diaHoras.get(4).getHora());
        cbMinViernes.setSelectedItem(diaHoras.get(4).getMin());

        cbHoraSabado.setSelectedItem(diaHoras.get(5).getHora());
        cbMinSabado.setSelectedItem(diaHoras.get(5).getMin());

        cbHoraDomingo.setSelectedItem(diaHoras.get(6).getHora());
        cbMinDomingo.setSelectedItem(diaHoras.get(6).getMin());

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        pack();
        setLocationRelativeTo(null);
        setTitle("Seleccione el momento de las actualizaciones Automaticas.");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Metodo que se encarga de gestionar el boton ok pulsado.
     */
    private void onOK() {
        listaDatos = new ArrayList<>();
        DiaHora diaHora = new DiaHora("MON", (String)cbHoraLunes.getSelectedItem(), (String) cbMinLunes.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("TUE", (String)cbHoraMartes.getSelectedItem(), (String)cbMinMartes.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("WED", (String)cbHoraMiercoles.getSelectedItem(), (String)cbMinMiercoles.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("THU", (String)cbHoraJueves.getSelectedItem(), (String) cbMinJueves.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("FRI", (String)cbHoraViernes.getSelectedItem(), (String) cbMinViernes.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("SAT", (String)cbHoraSabado.getSelectedItem(), (String) cbMinSabado.getSelectedItem());
        listaDatos.add(diaHora);
        diaHora = new DiaHora("SUN", (String)cbHoraDomingo.getSelectedItem(), (String) cbMinDomingo.getSelectedItem());
        listaDatos.add(diaHora);

        dispose();
    }

    /**
     * Metodo que se encarga de gestionar el boton cancelar pulsado.
     */
    private void onCancel() {
        cancelar = true;
        dispose();
    }

    /**
     * Metodo que se encarga de comunicar si ha sido pulsado o no el boton cancelar.
     * @return un valor booleano que comunica si ha sido o no pulsado el boton cancelar.
     */
    public boolean obtenerCancelar(){
        return cancelar;
    }
}
