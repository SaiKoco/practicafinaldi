package com.simion.gui;

import org.jfree.chart.ChartPanel;
import org.swixml.SwingEngine;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.io.File;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


/**
 * Clase que se encarga de mostrar el interfaz gráfico del programa.
 * Created by Simion on 04/03/2017.
 * @author Simion Cornel Bordean
 * @version 1.8.0_121
 */
public class Ventana {
    JFrame frame;
    JPanel pnlPrincipal;
    JSplitPane splitPane;
    JScrollPane scrollPane;
    JTable tbTabla;
    JPanel pnlGrafico;
    ChartPanel chartPanel;

    DefaultTableModel dtmTabla;

    JMenuBar menuBar;
    JMenu menuConectar;
    JMenu menuSeguridad;
    JMenu menuAyuda;
    JMenuItem conectarItem;
    JMenuItem autoBackUp;
    JMenuItem salirItem;
    JMenuItem crearBackUp;
    JMenuItem volcarBackUp;
    JMenuItem carpetaBackUp;
    JMenuItem contenidoBackUp;
    JMenuItem acercaDe;

    /**
     * Constructor de la clase.
     */
    public Ventana(){
        frame = new JFrame("Practica final DI");

        chartPanel = new ChartPanel(null);
        chartPanel.setToolTipText("Espacio para el Gráfico");
        pnlGrafico.add(chartPanel);

        try {
            new SwingEngine(this).render("menuBar.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }frame.setJMenuBar(menuBar);
        frame.setContentPane(pnlPrincipal);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(800, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtmTabla = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dtmTabla.addColumn("Bases de datos existentes");

        tbTabla.setModel(dtmTabla);
    }

    /**
     * Metodo que obtiene la ruta de un fichero que se quiere abrir.
     * @return un String con el valor del ruta del path completo
     */
    public String obtenerAbrir() {
        JFileChooser jFileChooser = new JFileChooser("backup/");
        jFileChooser.showOpenDialog(carpetaBackUp);
        return jFileChooser.getSelectedFile().toString();
    }

    /**
     * Metodo que obtiene la ruta de la carpeta donde se desea guardar los BackUps.
     * @return un String con la ruta completa hasta la carpeta seleccionada.
     */
    public String obtenerDirectorio() {
        JFileChooser jFileChooser = new JFileChooser("backup/");
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jFileChooser.showOpenDialog(carpetaBackUp);
        return jFileChooser.getSelectedFile().toString();
    }
}
