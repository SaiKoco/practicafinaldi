import com.simion.gui.Controler;
import com.simion.gui.Model;
import com.simion.gui.Ventana;

/**
 * Clase Principal de la aplicación donde se genera el model MVC.
 * Created by Simion on 04/03/2017.
 * @author Simion Cornel Bordean.
 * @version 1.8.0_121
 */
public class Aplicacion {

    /**
     * Metodo principal de la aplicación.
     * @param args parametros extra al ejecutar la aplicación.
     */
    public static void main(String[] args){
        Model modelo = new Model();
        Ventana view = new Ventana();
        new Controler(modelo, view);
    }
}
